﻿using UnityEngine;

public class BitMaskAttribute : PropertyAttribute
{
    public System.Type propType;
    public bool drawNames;

    public BitMaskAttribute(System.Type aType, bool drawnames = false)
    {
        propType = aType;
        drawNames = drawnames;
    }
}
