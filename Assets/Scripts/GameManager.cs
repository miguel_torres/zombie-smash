﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public Camera Camera
    {
        get
        {
            return _camera;
        }
    }

    private Camera _camera;
    private Transform _cameraContainer;

    [Header("Camera")]
    [SerializeField]
    private float _cameraOffset;

    [Space]
    [SerializeField]
    private GameSetup _gameSetup;

    private PlayerController _player;

    //Camera shake
    [Space]
    [Header("Camera Shake")]
    [SerializeField]
    private bool smooth;
    [SerializeField]
    private float smoothAmount = 5f;
    private float shakeAmount;
    private float shakeDuration;
    float shakePercentage;
    float startAmount;
    float startDuration;

    bool isRunning = false;
    private float _stopTime;
    

    public PlayerController Player
    {
        get
        {
            return _player;
        }
    }

    public GameSetup GameSetup
    {
        get
        {
            return _gameSetup;
        }
    }

    private Dictionary<Collider, Character> _characterColliders = new Dictionary<Collider, Character>();
    private List<Character>[] _characterLayers = new List<Character>[16];
    private List<Character> _worldCharacters = new List<Character>();
    private List<AIController> _worldAIs = new List<AIController>();

    private void Awake()
    {
        _instance = this;
        _camera = Camera.main;
        _cameraContainer = _camera.transform.parent;

        for (int i = 0; i < _characterLayers.Length; i++)
        {
            _characterLayers[i] = new List<Character>();
        }
    }

    private void Start()
    {
        _player = FindObjectOfType<PlayerController>();
        Application.targetFrameRate = 60;

        Resolution res = Screen.currentResolution;
        res.height = (int)(res.height * GameSetup.Resolution);
        res.width = (int)(res.width * GameSetup.Resolution);
        Screen.SetResolution(res.width, res.height, true);
    }

    public void AddCharacter(Collider collider, Character character)
    {
        _characterColliders.Add(collider, character);
        _characterLayers[character.gameObject.layer].Add(character);

        if (!_worldCharacters.Contains(character))
        {
            _worldCharacters.Add(character);
        }
    }

    public void AddAI(AIController ai)
    {
        if (!_worldAIs.Contains(ai))
        {
            _worldAIs.Add(ai);
        }
    }

    public Character GetNearestEnemy(Character character, LayerMask layer)
    {
        List<Character>[] layers = _characterLayers;
        float nearest = Mathf.Infinity;
        Character nearestChar = null;

        for (int l = 0; l < layers.Length; l++)
        {
            //print("Checking layer: " + l);
            if (layer  == (layer | (1 << l)))
            {
                //print("Layer is target");
                List<Character> enemies = layers[l];
                for (int i = 0; i < enemies.Count; i++)
                {
                    //print("Checking enemy: " + enemies[i]);
                    if (enemies[i] == character)
                    {
                        //print("Dont attack self");
                        continue;
                    }

                    float dist = (enemies[i].transform.position - character.transform.position).sqrMagnitude;
                    if (dist < nearest)
                    {
                        nearest = dist;
                        nearestChar = enemies[i];
                    }
                }
            }
        }

        return nearestChar;
    }

    public Character GetCharacter(Collider collider)
    {
        return _characterColliders[collider];
    }

    public void ShowDamage(Vector3 position)
    {
        Instantiate(GameSetup.DamageParticles, position, Quaternion.identity);
    }

    //++Effects
    public void ShakeCamera(float amount, float shakeMultiplier = 1, float stopTime = 0)
    {
        float shake = amount * .2f;

        if (_stopTime <= 0)
        {
            _stopTime = stopTime;
        }
        //print("Time stopped for: " + _stopTime);
        Shake(shake * 100 * shakeMultiplier, shake * 5);
    }

    public void Shake(float amount, float duration)
    {
        shakeAmount = amount;
        startAmount = shakeAmount;
        shakeDuration = duration;
        startDuration = shakeDuration;

        if (!isRunning) StartCoroutine(Shake());
    }

    IEnumerator Shake()
    {
        isRunning = true;

        while (shakeDuration > 0)
        {
            Vector3 rotationAmount = Random.insideUnitSphere * shakeAmount;
            rotationAmount.z = 0;

            shakePercentage = shakeDuration / startDuration;

            shakeAmount = startAmount * shakePercentage;
            shakeDuration -= Time.unscaledDeltaTime;

            if (smooth)
                _camera.transform.localRotation = Quaternion.Lerp(_camera.transform.localRotation, Quaternion.Euler(rotationAmount), Time.unscaledDeltaTime * smoothAmount);
            else
                _camera.transform.localRotation = Quaternion.Euler(rotationAmount);

            yield return null;
        }
        _camera.transform.localRotation = Quaternion.identity;
        isRunning = false;
    }

    private void Update()
    {
        if (_stopTime > 0)
        {
            Time.timeScale = .001f;
            _stopTime -= Time.unscaledDeltaTime;
        }
        else
        {
            Time.timeScale = 1;
        }

        _cameraContainer.transform.position = _player.transform.position;
        _cameraContainer.transform.Translate(-_cameraContainer.transform.forward * _cameraOffset, Space.World);

        for(int i = 0; i < _worldCharacters.Count; i++)
        {
            _worldCharacters[i].ManagedUpdate();
        }
        for (int i = 0; i < _worldAIs.Count; i++)
        {
            _worldAIs[i].ManagedUpdate();
        }
    }

    private void FixedUpdate()
    {
        for (int i = 0; i < _worldCharacters.Count; i++)
        {
            _worldCharacters[i].ManagedFixedUpdate();
        }
        for (int i = 0; i < _worldAIs.Count; i++)
        {
            _worldAIs[i].ManagedFixedUpdate();
        }
    }
}
