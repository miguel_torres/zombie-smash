﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Character : MonoBehaviour 
{
    private enum state { Idle, Walking, Jumping, Attacking, Hurt, Down }
    public enum attackType { Normal, Launcher }

    private CharacterController _controller;
    private CapsuleCollider _triggerCollider;
    private NavMeshAgent _nav;
    private Vector2 _input;
    private Vector3 _movement;
    private Animator _animator;
    private Vector3 _extraForce;
    private float _stun;
    private float _health;//Accumulated damage

    [Header("Stats")]
    [Tooltip("Damage multiplier")]
    [SerializeField]
    private float _damage = 1;
    [SerializeField]
    [Tooltip("Resistance to damage")]
    private float _resistance;
    [SerializeField]
    private float _downRecoverTime = 1;
    private float _currentDownRecover;
    private bool _downDamage;

    private string _animationSpeedString = "Speed";
    private string _animationJumpString = "Jump";
    private string _animationAttackString = "Attack";
    private string _animationDamageString = "Damage";
    private string _animationHeighString = "HeighVelocity";    
    private int _animationSpeedHash;
    private int _animationJumpHash;
    private int _animationAttackHash;
    private int _animationDamageHash;
    private int _animationHeighHash;

    private const float _switchColliderHeight = .17f;

    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _rotationSpeed;
    [SerializeField]
    private float _jumpForce;
    [SerializeField]
    private float _friction = 1;
    [SerializeField]
    private Move[] _normalAttacks;
    [SerializeField]
    private Move[] _airAttacks;
    [SerializeField]
    private Move[] _launcherAttacks;
    [SerializeField]
    [Tooltip("Run time to trigger dash attack")]
    private float _dashMoveTime = 2;   
    private float _movementTime;
    [SerializeField]
    private Move[] _dashAttacks;
    private Move _currentMove;
    private bool _bufferMove;
    private attackType _bufferMoveType;
    private Move.MoveInstance _currentMoveInstance;

    [SerializeField]
    private LayerMask _groundLayer;
    [SerializeField]
    private LayerMask _targersLayer;
    [SerializeField]
    private Transform _shadow;

    private Vector3 _previousPosition;
    private RaycastHit[] _groundHit = new RaycastHit[2];
    private int _platformed;
    private state _state;
    private bool _bounced;
    private bool _animateHeigh;

    private int _characterLayer;
    public Vector3 _debugMovePosition;
    public float _debugMoveRadius;

    public Vector2 Input
    {
        get
        {
            return _input;
        }

        set
        {
            _input = value;
        }
    }

    public LayerMask TargetsLayer
    {
        get
        {
            return _targersLayer;
        }
    }

    public bool Stunned
    {
        get
        {
            if(_state == state.Hurt || _state == state.Down)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public float Speed
    {
        get
        {
            return _speed;
        }
    }

    public int CharacterLayer
    {
        get
        {
            return _characterLayer;
        }
    }

    private void Awake()
    {
        _controller = GetComponent<CharacterController>();
        _animator = GetComponentInChildren<Animator>();
        _nav = GetComponent<NavMeshAgent>();

        if (_nav)
        {
            _nav.updatePosition = false;
            _nav.updateRotation = false;
        }

        _animationSpeedHash = Animator.StringToHash(_animationSpeedString);
        _animationJumpHash = Animator.StringToHash(_animationJumpString);
        _animationAttackHash = Animator.StringToHash(_animationAttackString);
        _animationDamageHash = Animator.StringToHash(_animationDamageString);
        _animationHeighHash = Animator.StringToHash(_animationHeighString);

        _characterLayer = gameObject.layer;
    }

    private void Start()
    {
        GameManager.Instance.AddCharacter(GetComponent<Collider>(), this);
        _previousPosition = transform.position;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(_state == state.Hurt && !_bounced)
        {
            if (_groundLayer == (_groundLayer | (1 << hit.gameObject.layer)))
            {
                //print("Hit wall: " + hit.normal);
                Vector3 bounce = hit.normal;
                _movement = Vector3.zero;
                _extraForce = Vector3.Reflect(_controller.velocity, bounce);
                Vector3 reflectForce = _extraForce;
                
                _extraForce.x *= .75f;
                _extraForce.y *= .5f;
                _extraForce.z *= .75f;

                _bounced = true;

                //print("Reflect: " + reflectForce.magnitude);
                if (reflectForce.magnitude >= GameManager.Instance.GameSetup.BounceAnimationThreshold)
                {
                    _animator.SetInteger(_animationDamageHash, GameManager.Instance.GameSetup.BounceAnimationId);
                    _animateHeigh = false;
                }
            }
        }
    }

    public void MoveForward(float amount)
    {
        if (_state != state.Walking && _state != state.Idle && _state != state.Jumping)
        {
            return;
        }

        if(Stunned)
        {
            return;
        }

        if(_currentMoveInstance != null)
        {
            return;
        }

        amount = Mathf.Clamp(amount, -1, 1);
        Vector3 direction = transform.forward * amount * Speed;

        _input.x = amount;
        _movement.x = direction.x;
        _movement.z = direction.z;

        if(_controller.isGrounded)
        {
            if (amount == 0)
            {
                _state = state.Idle;
            }
            else
            {
                _state = state.Walking;
            }
        }

        //_rigidBody.velocity = new Vector3(direction.x, _rigidBody.velocity.y, direction.z);
    }

    /// <summary>
    /// Move and rotate character
    /// </summary>
    /// <param name="direction"></param>
    public void Move(Vector2 direction)
    {
        MoveForward(direction.magnitude);

        if (direction != Vector2.zero)
        {
            Quaternion lookRot = Quaternion.LookRotation(new Vector3(direction.x, transform.position.y, direction.y));
            SetRotation(lookRot);
        }
    }

    public void Rotate(float rotation)
    {
        if (Stunned)
        {
            return;
        }

        if (_currentMoveInstance != null)
        {
            if (!_currentMoveInstance.AllowRotation)
            {
                return;
            }
        }

        transform.Rotate(new Vector3(0, rotation * _rotationSpeed * Time.deltaTime, 0));
    }

    public void SetRotation(Quaternion rotation)
    {
        if (Stunned)
        {
            return;
        }

        if(_currentMoveInstance != null)
        {
            if (!_currentMoveInstance.AllowRotation)
            {
                return;
            }
        }

        Quaternion fixedrotation = Quaternion.Slerp(transform.rotation, rotation, _rotationSpeed * Time.fixedDeltaTime);
        fixedrotation.x = 0;
        fixedrotation.z = 0;
        transform.rotation = fixedrotation;
    }

    public void Jump()
    {
        if (_controller.isGrounded && _state != state.Attacking && !Stunned)
        {
            _movement.y = _jumpForce;
            _animator.SetInteger(_animationJumpHash, 1);
            _state = state.Jumping;
        }
    }

    public void Attack(attackType type)
    {
        if(Stunned)
        {
            return;
        }

        if(_state == state.Attacking)
        {
            if(_currentMoveInstance != null)
            {
                if (!_currentMoveInstance.Linkable)
                {
                    _bufferMove = true;
                    _bufferMoveType = type;
                    return;
                }
                else
                {
                    KillCurrentMove();
                }
            }
        }

        _bufferMove = false;

        Move[] movelist = new Move[0];

        if (_controller.isGrounded)
        {
            movelist = _normalAttacks;
            if (_movementTime >= _dashMoveTime && _dashAttacks.Length > 0)
            {
                movelist = _dashAttacks;
            }
            else if (type == attackType.Launcher)
            {
                movelist = _launcherAttacks;
            }
        }
        else if(_airAttacks.Length > 0)
        {
            movelist = _airAttacks;
        }

        if(movelist.Length > 0)
        {
            _state = state.Attacking;

            _currentMove = movelist[Random.Range(0, movelist.Length)];
            _currentMoveInstance = new Move.MoveInstance(this, _currentMove.Hits, _targersLayer, _currentMove.Duration, _currentMove.LinkTime, _damage, _currentMove.CancelOnLanding, _currentMove.AllowRotation);
            _animator.Play(_currentMove.AnimationName, -1, 0);

            if (_currentMove.ResetSelfForce)
            {
                _movement = Vector3.zero;
            }
            _movement += transform.forward * _currentMove.SelfAppliedForce.x;
            _movement += transform.up * _currentMove.SelfAppliedForce.y;
            _movementTime = 0;
        }
        
    }

    private void KillCurrentMove()
    {
        if (_currentMoveInstance != null)
        {
            _movementTime = 0;
            _currentMoveInstance.Finish();
        }
        _currentMove = null;
        _currentMoveInstance = null;
    }

    IEnumerator nextAttackAnim(string animationName)
    {
        yield return new WaitForEndOfFrame();
        _animator.CrossFade(animationName, 0.05f, -1);
    }

    public void Damage(Character origin, float damage, Vector3 force, bool resetForce, float stunTime)
    {
        //print("Damage");
        if (resetForce)
        {
            _extraForce = Vector3.zero;
            _movement = Vector3.zero;
        }
        KillCurrentMove();

        damage -= _resistance;

        //Chip damage
        if(damage <= 0)
        {
            damage = .1f;
        }

        _health += damage;
        _bufferMove = false;
        _extraForce += force * _health;

        //Damage type
        //print("Force: " + _extraForce.magnitude);
        GameSetup.DamageType damageType = GameManager.Instance.GameSetup.GetDamageType(_extraForce.magnitude);

        //Effects
        damageType.ShakeCamera();

        if (damageType.HitEffect)
        {
            GameObject damageEffect = Instantiate(damageType.HitEffect, transform);
            damageEffect.transform.localPosition = Vector3.zero;
        }

        _animateHeigh = damageType.AnimateHeigh;
        _bounced = false;
        _downDamage = damageType.LandDown;
        _stun = stunTime * damageType.StunMultiplier;
        _animator.SetInteger(_animationDamageHash, damageType.AnimationId);
        _state = state.Hurt;        
    }

    private void Drag()
    {
        if (_controller.isGrounded)
        {
            NullifyValue(ref _extraForce.x, _friction * Time.deltaTime);
            NullifyValue(ref _extraForce.z, _friction * Time.deltaTime);
        }
        
        NullifyValue(ref _extraForce.y, _friction * Time.deltaTime);
        
    }

    private void NullifyValue(ref float value, float speed)
    {
        if (value > 0)
        {
            value -= speed;
            if (value < 0)
            {
                value = 0;
            }
        }
        else if (value < 0)
        {
            value += speed;
            if (value > 0)
            {
                value = 0;
            }
        }
    }

    public void ManagedUpdate()
    {
        if(_stun > 0)
        {
            if (_controller.isGrounded)
            {
                _animator.SetInteger(_animationJumpHash, 0);
            }
            else if (_movement.y > 0 || _extraForce.y > 0)
            {
                _animator.SetInteger(_animationJumpHash, 1);
            }

            if (!_controller.isGrounded && _animateHeigh)
            {
                _animator.SetFloat(_animationHeighHash, (_controller.velocity).normalized.y);
            }
            return;
        }
        else
        {
            _animator.SetFloat(_animationHeighHash, 1);
            _animator.SetInteger(_animationDamageHash, 0);
        }

        if (_controller.isGrounded)
        {
            _animator.SetFloat(_animationHeighHash, 1);
        }

        _animator.SetFloat(_animationSpeedHash, _input.x);

        if (!_controller.isGrounded && _state != state.Attacking)
        {
            if (_movement.y < 0)
            {
                _animator.SetInteger(_animationJumpHash, -1);
            }
            else if(_movement.y > 0 || _extraForce.y > 0)
            {
                _animator.SetInteger(_animationJumpHash, 1);
            }
        }
        else if (_controller.isGrounded && _movement.y < 0)
        {
            _animator.SetInteger(_animationJumpHash, 0);
        }
    }

    public void ManagedFixedUpdate()
    {
        Drag();

        if (!_controller.isGrounded)
        {
            //_controller.radius = .01f;
            _movement.y += Physics.gravity.y * Time.deltaTime;
            //_extraForce.y += Physics.gravity.y * Time.deltaTime;
        }
        else
        {
            //_controller.radius = _originalRadius;
            //if(_extraForce.y < 0)
            //{
            //_extraForce.y = 0f;
            //}
            //_movement.y = -0.01f;
        }

        _controller.Move((_extraForce + _movement) * Time.deltaTime);

        if(_controller.velocity != Vector3.zero && _controller.isGrounded && _currentMove == null)
        {//If running
            _movementTime += Time.deltaTime;
        }
        else
        {
            _movementTime = 0;
        }

        _platformed = Physics.RaycastNonAlloc(transform.position, Vector3.down, _groundHit, 1000, _groundLayer);

        if (_platformed == 0)   //the player is on the platform
        {
            if (_health == 0)
            transform.position = new Vector3(_previousPosition.x, transform.position.y, _previousPosition.z);
        }
        else
        {
            /*if (CompareTag("Player"))
            {
                print("Ground dist: " + _groundHit[0].distance);
            }*/
            //Collider switch
            if (_groundHit[0].distance > _switchColliderHeight)
            {
                gameObject.layer = GameManager.Instance.GameSetup.AirLayer.LayerIndex;
            }
            else
            {
                gameObject.layer = _characterLayer;
            }

            if (_shadow)
            {
                _shadow.position = _groundHit[0].point;
            }
        }

        if (_currentMoveInstance != null)
        {            
            if (_controller.isGrounded)
            {
                _currentMoveInstance.LandCancel();
            }

            _currentMoveInstance.Perform();
            if (_currentMoveInstance.Finished)
            {
                _currentMove = null;
                _currentMoveInstance = null;
                //Destroy(_currentMoveInstance);
                _animator.Play("Idle");
                //_animator.CrossFade("Idle", 0.1f, -1);
                _state = state.Idle;

                if (_bufferMove)
                {
                    Attack(_bufferMoveType);
                }
            }
        }

        if(_stun > 0)
        {
            float recoverMul = 1;

            if (_controller.isGrounded)
            {
                //Recover faster in ground
                recoverMul = 3;

                if (_downDamage)
                {
                    _state = state.Down;
                    _currentDownRecover = _downRecoverTime;
                }
            }
            else
            {
                _state = state.Hurt;
            }
            _stun -= Time.deltaTime * recoverMul;
            

        }
        else if(_state == state.Down)
        {
            _currentDownRecover -= Time.deltaTime;
            if(_currentDownRecover <= 0)
            {
                _state = state.Idle;
            }
        }
        else if (_state == state.Hurt)
        {
            _state = state.Idle;
        }

        _previousPosition = transform.position;
    }

    private void OnDrawGizmos()
    {
        if (_currentMoveInstance != null)
        {
            _currentMoveInstance.Perform();
            if (!_currentMoveInstance.Finished)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(_debugMovePosition, _debugMoveRadius);
            }
        }        
    }
}
