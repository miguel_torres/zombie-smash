﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetup : ScriptableObject 
{
    [System.Serializable]
    public class DamageType
    {
        [SerializeField]
        private string _name;
        [SerializeField]
        private int _animationId;
        [SerializeField]
        private float _forceThreshold;
        [SerializeField]
        private float _stunMultiplier;
        [SerializeField]
        private GameObject _hitEffect;
        [Space]
        [Header("Effects")]
        [SerializeField]
        private float _timeStop;
        [SerializeField]
        private float _cameraShake;
        [SerializeField]
        [Tooltip("Make the target enter down state after landing")]
        private bool _landDown;
        [SerializeField]
        private bool _animateHeigh;

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public int AnimationId
        {
            get
            {
                return _animationId;
            }
        }

        public float ForceThreshold
        {
            get
            {
                return _forceThreshold;
            }
        }

        public float StunMultiplier
        {
            get
            {
                return _stunMultiplier;
            }
        }

        public GameObject HitEffect
        {
            get
            {
                return _hitEffect;
            }
        }

        public bool LandDown
        {
            get
            {
                return _landDown;
            }
        }

        public bool AnimateHeigh
        {
            get
            {
                return _animateHeigh;
            }
        }

        public void ShakeCamera()
        {
            GameManager.Instance.ShakeCamera(_cameraShake, 1, _timeStop);
        }
    }

    [SerializeField]
    private GameObject _damageParticles;
    [SerializeField]
    private float _bounceAnimationThreshold;
    [SerializeField]
    private int _bounceAnimationId;
    [SerializeField]
    private SingleUnityLayer _airLayer;
    [SerializeField]
    private DamageType[] _damageTypes;

    [Space]
    [Header("Settings")]
    [SerializeField]
    [Range(.5f, 1)]
    private float _resolution = 1;

    public GameObject DamageParticles
    {
        get
        {
            return _damageParticles;
        }
    }

    public DamageType[] DamageTypes
    {
        get
        {
            return _damageTypes;
        }
    }

    public float Resolution
    {
        get
        {
            return _resolution;
        }
    }

    public int BounceAnimationId
    {
        get
        {
            return _bounceAnimationId;
        }
    }

    public float BounceAnimationThreshold
    {
        get
        {
            return _bounceAnimationThreshold;
        }
    }

    public SingleUnityLayer AirLayer
    {
        get
        {
            return _airLayer;
        }
    }

    public DamageType GetDamageType(float force)
    {
        DamageType dam = _damageTypes[0];//Default damage

        for(int i = 0; i < _damageTypes.Length; i++)
        {
            if (force < _damageTypes[i].ForceThreshold)
            {
                break;
            }
            dam = _damageTypes[i];
        }

        return dam;
    }
}
