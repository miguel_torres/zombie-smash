﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : ScriptableObject 
{
    [System.Serializable]
    public class Hit
    {
        [SerializeField]
        private Vector3 _position;
        [SerializeField]
        private float _hitsFrequency;
        [SerializeField]
        private float _hitboxSize;
        [SerializeField]
        private float _damageMultiplier;
        [SerializeField]
        private bool _resetForces;
        [SerializeField]
        private Vector2 _force;
        [SerializeField]
        private float _stunTime;

        public Vector3 Position
        {
            get
            {
                return _position;
            }
        }

        public float HitsFrequency
        {
            get
            {
                return _hitsFrequency;
            }
        }

        public float HitboxSize
        {
            get
            {
                return _hitboxSize;
            }
        }

        public float DamageMultiplier
        {
            get
            {
                return _damageMultiplier;
            }
        }

        public Vector2 Force
        {
            get
            {
                return _force;
            }
        }

        public float StunTime
        {
            get
            {
                return _stunTime;
            }
        }

        public bool ResetForces
        {
            get
            {
                return _resetForces;
            }
        }
    }

    [System.Serializable]
    public class HitData
    {
        [SerializeField]
        private Hit _hit;
        [SerializeField]
        private float _time;

        public Hit Hit
        {
            get
            {
                return _hit;
            }
        }

        public float Time
        {
            get
            {
                return _time;
            }
        }
    }

    [System.Serializable]
    public class MoveInstance
    {
        private Character _caster;
        private HitData[] _hits;
        private float _duration;
        private float _linkTime;
        private float _currentTime;
        private int _currentHitIndex;
        private LayerMask _targersLayer;
        private List<Character> _targets = new List<Character>();
        private List<HitData> _pendingHits = new List<HitData>();
        private Collider[] _rayHits = new Collider[10];
        private float _damageMultiplier;
        private bool _landingCancel;
        private bool _allowRotation;

        public bool Finished
        {
            get
            {
                if (_currentTime >= _duration)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool Linkable
        {
            get
            {
                if (_currentTime >= _linkTime)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool AllowRotation
        {
            get
            {
                return _allowRotation;
            }

        }

        public MoveInstance(Character caster, HitData[] hits, LayerMask layer, float duration, float linkTime, float damageMultiplier, bool landCancel, bool allowRotation)
        {
            _caster = caster;
            _hits = hits;
            _targersLayer = layer;
            _targersLayer |= (1 << GameManager.Instance.GameSetup.AirLayer.LayerIndex);

            _duration = duration;
            _linkTime = linkTime;
            _damageMultiplier = damageMultiplier;
            _landingCancel = landCancel;
            _allowRotation = allowRotation;
            _pendingHits = new List<HitData>();

            _caster._debugMovePosition = Vector3.positiveInfinity;
            _caster._debugMoveRadius = 0;
        }

        public void Perform()
        {
            if (Finished)
            {
                return;
            }
            
            _currentTime += Time.deltaTime;
            //_pendingHits = new List<HitData>();
            _pendingHits.Clear();

            for (int i = _currentHitIndex; i < _hits.Length; i++)
            {
                if(_currentTime >= _hits[i].Time)
                {
                    _pendingHits.Add(_hits[i]);
                    _currentHitIndex = i + 1;
                }
                else
                {
                    _currentHitIndex = i;
                    break;
                }
            }

            //Debug.Log("Pending hits: " + _pendingHits.Count);
            for(int i = 0; i < _pendingHits.Count; i++)
            {
                //Debug.Log("Casting");
                CastHit(_pendingHits[i]);
            }
        }

        public void LandCancel()
        {
            if (_landingCancel)
            {
                Finish();
            }
        }

        public void Finish()
        {
            _currentTime = 10000;
        }

        public Vector3 HitOffset(Hit hit)
        {
            Vector3 offset = _caster.transform.forward * hit.Position.x;
            offset += _caster.transform.right * hit.Position.z;
            offset += _caster.transform.up * hit.Position.y;

            return _caster.transform.position + offset;
        }

        private void CastHit(HitData hit)
        {
            Vector3 hitPos = HitOffset(hit.Hit);
            int hits = Physics.OverlapSphereNonAlloc(hitPos, hit.Hit.HitboxSize, _rayHits, _targersLayer);

            for(int i = 0; i < hits; i++)
            {
                Character character = GameManager.Instance.GetCharacter(_rayHits[i]);

                //Dont hurt himself and team members
                if(character == _caster)
                {
                    continue;
                }
                if (_caster.TargetsLayer != (_caster.TargetsLayer | (1 << character.CharacterLayer)))
                {
                    continue;
                }

                Vector3 characterPos = character.transform.position;
                characterPos.y = _caster.transform.position.y;

                if (_caster.transform.position != character.transform.position)
                {
                    Vector3 targetDirection = _caster.transform.position - character.transform.position;
                    character.SetRotation(Quaternion.LookRotation(targetDirection));
                    //character.SetRotation(_caster.transform.position - character.transform.position).normalized * 1000);
                }

                Vector3 direction = (character.transform.position - _caster.transform.position).normalized * hit.Hit.Force.x;
                direction.y = hit.Hit.Force.y;

                character.Damage(_caster, hit.Hit.DamageMultiplier * _damageMultiplier, direction, hit.Hit.ResetForces, hit.Hit.StunTime);
                GameManager.Instance.ShowDamage(hitPos);
            }

            //Debug.Log("Casted");
            _caster._debugMovePosition = hitPos;
            _caster._debugMoveRadius = hit.Hit.HitboxSize;
        }

    }

    [SerializeField]
    private string _name;
    [SerializeField]
    private string _animationName;
    [SerializeField]
    private HitData[] _hits;
    [SerializeField]
    private Vector2 _selfAppliedForce;
    [SerializeField]
    private bool _resetSelfForce;
    [SerializeField]
    private float _duration;
    [SerializeField]
    private float _linkTime;
    [SerializeField]
    private bool _cancelOnLanding;
    [SerializeField]
    [Tooltip("Allow the character to use rotation input")]
    private bool _allowRotation = true;

    public string AnimationName
    {
        get
        {
            return _animationName;
        }
    }

    public HitData[] Hits
    {
        get
        {
            return _hits;
        }
    }

    public float Duration
    {
        get
        {
            return _duration;
        }
    }

    public Vector2 SelfAppliedForce
    {
        get
        {
            return _selfAppliedForce;
        }
    }

    public bool ResetSelfForce
    {
        get
        {
            return _resetSelfForce;
        }
    }

    public float LinkTime
    {
        get
        {
            return _linkTime;
        }
    }

    public bool CancelOnLanding
    {
        get
        {
            return _cancelOnLanding;
        }
    }

    public bool AllowRotation
    {
        get
        {
            return _allowRotation;
        }
    }
}
