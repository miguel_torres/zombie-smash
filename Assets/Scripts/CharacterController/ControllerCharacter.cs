﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerCharacter : MonoBehaviour 
{
    protected Character _character;
    protected Vector2 _input;
    protected bool _jump;

    protected virtual void Awake()
    {
        _character = GetComponent<Character>();
    }
}
