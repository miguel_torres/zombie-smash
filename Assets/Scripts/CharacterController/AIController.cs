﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : ControllerCharacter {

    private NavMeshAgent _nav;
    [SerializeField]
    private float _attackDistance;
    private Character _target;
    [SerializeField]
    [Range(0, 1)]
    private float _launcherFrequency;
    [SerializeField]
    [Range(0,1)]
    private float _jumpFrequency;
    private float _currentJumpFrequency;
    [SerializeField]
    [Range(0, 2)]
    private float _thinkFrequency = .5f;
    private float _currentThinkFrequency;
    private bool _canJump;

    protected override void Awake()
    {
        base.Awake();
        _nav = GetComponent<NavMeshAgent>();

        _nav.updatePosition = false;
        _nav.updateRotation = false;

        _currentThinkFrequency = Random.Range(0, _thinkFrequency);
    }

    private void Start()
    {
        GameManager.Instance.AddAI(this);
    }

    public void ManagedUpdate()
    {
        if(_currentThinkFrequency > 0)
        {
            return;
        }

        _currentThinkFrequency = Random.Range(0, _thinkFrequency);

        if (_character.Stunned)
        {
            _nav.nextPosition = transform.position;
            return;
        }

        //Vector3 targetPos = GameManager.Instance.Player.transform.position;
        _target = GameManager.Instance.GetNearestEnemy(_character, _character.TargetsLayer);

        if (!_target)
        {
            return;
        }

        Vector3 targetPos = _target.transform.position;

        float targetDistance = Vector3.Distance(targetPos, _character.transform.position);

        if(targetDistance <= _attackDistance)
        {
            Vector3 targetDirection = targetPos - _character.transform.position;
            _character.SetRotation(Quaternion.LookRotation(targetDirection));
            _character.MoveForward(0);

            Character.attackType attacktype = Character.attackType.Normal;

            if(Random.Range(.01f,1) < _launcherFrequency)
            {
                attacktype = Character.attackType.Launcher;
            }

            _character.Attack(attacktype);
            return;
        }

        _nav.SetDestination(targetPos);

        Vector3 normPos = _character.transform.position;
        normPos.y = _nav.nextPosition.y;
        float navdistance = (_nav.nextPosition - normPos).magnitude;
        /*if (navdistance > 1)
        {
            _nav.speed = 1000;
            //_navAgent.nextPosition = _character.Transform.position;
        }*/

        if(navdistance > 1f)
        {
            _nav.nextPosition = transform.position;
        }
        else if (navdistance > .6f)
        {
            _nav.speed = 0;
        }
        else
        {
            _nav.speed = _character.Speed;
        }

        //_nav.nextPosition.y = _character.transform.position.y;
        Vector3 direction = _nav.nextPosition - _character.transform.position;
        _character.SetRotation(Quaternion.LookRotation(direction));
        _character.MoveForward(1);

        //Jump
        if (Random.Range(.01f, 1) < _jumpFrequency)
        {
            _character.Jump();
        }
    }

    public void ManagedFixedUpdate()
    {
        _currentThinkFrequency -= Time.deltaTime;        
    }
}
