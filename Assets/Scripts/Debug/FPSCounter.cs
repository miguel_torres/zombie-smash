﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour 
{
    [SerializeField]
    private Text _text;
    [SerializeField]
    private float UpdateFrequency = 1;
    [SerializeField]
    private UnityEngine.Gradient _colors;
    private float timer;

    private float deltaTime;

    void countfps()
    {
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        _text.text = text;
        Color color = _colors.Evaluate(fps / 60);
        _text.color = color;
    }

    // Update is called once per frame
    void Update () {
        timer += Time.deltaTime;
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        if (timer >= UpdateFrequency)
        {
            timer = 0;
            countfps();
        }
	}
}
