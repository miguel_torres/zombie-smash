﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : ControllerCharacter {

    private readonly float _launcherThreshold = .5f;
    private float _currentLauncherThreshold;

#if UNITY_EDITOR
    private Vector3 debugMovement;
#endif

    private void Update()
    {
        _input.x = Input.GetAxis("Vertical");
        _input.y = Input.GetAxis("Horizontal");
        _jump = Input.GetButtonDown("Jump");

        if(_input == Vector2.zero)
        {
            _currentLauncherThreshold = 0;
        }
        else
        {
            _currentLauncherThreshold += Time.deltaTime;
        }

        _input.x = Mathf.Clamp(_input.x, -1, 1);
        _input.y = Mathf.Clamp(_input.y, -1, 1);

        //Relative to camera
        Vector3 forward = GameManager.Instance.Camera.transform.forward;
        Vector3 right = GameManager.Instance.Camera.transform.right;

        forward.y = 0f;
        right.y = 0f;
        forward.Normalize();
        right.Normalize();

        Vector3 moveDirection = (forward * _input.x) + (right * _input.y);

#if UNITY_EDITOR
        debugMovement = moveDirection;
#endif

        _character.Move( new Vector2(moveDirection.x, moveDirection.z));
        //_character.MoveForward(_input.x);        

        if (_jump)
        {
            _character.Jump();
        }

        if (Input.GetButtonDown("Fire1"))
        {
            Character.attackType attacktype = Character.attackType.Normal;

            if(_currentLauncherThreshold > 0 && _currentLauncherThreshold < _launcherThreshold)
            {
                attacktype = Character.attackType.Launcher;
            }

            _character.Attack(attacktype);
        }
    }

    private void OnDrawGizmos()
    {
#if UNITY_EDITOR
        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(transform.position, debugMovement);
#endif
    }
}
